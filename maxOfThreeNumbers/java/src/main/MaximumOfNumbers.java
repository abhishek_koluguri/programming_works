import java.util.ArrayList;

public class MaximumOfNumbers {

    public int max(ArrayList<Integer> numbers){
        int highest = numbers.get(0);

        for(int i = 0; i < numbers.size(); i++) {
            if(highest < numbers.get(i))
                highest = numbers.get(i);
        }
        return highest;
    }
}
