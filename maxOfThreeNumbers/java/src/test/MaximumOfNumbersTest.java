import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MaximumOfNumbersTest {

    MaximumOfNumbers maximumOfNumbers;
    ArrayList<Integer> numbers;

    @Before
    public void setUp(){
        maximumOfNumbers = new MaximumOfNumbers();
        numbers = new ArrayList<>();
    }

    @Test
    public void canary(){
        assertTrue(true);
    }

    @Test
    public void testForOnlyOneNumber(){
        numbers.add(2);
        assertEquals(2, maximumOfNumbers.max(numbers));
    }

    @Test
    public void testForOnlyTwoNumbers(){
        numbers.add(1);
        numbers.add(4);
        assertEquals(4, maximumOfNumbers.max(numbers));
    }

    @Test
    public void testForSameNumbers(){
        numbers.add(0);
        numbers.add(0);
        numbers.add(0);
        assertEquals(0, maximumOfNumbers.max(numbers));
    }

    @Test
    public void testForAllNegatives(){
        numbers.add(-10);
        numbers.add(-9);
        numbers.add(-1);
        assertEquals(-1, maximumOfNumbers.max(numbers));
    }

    @Test
    public void testForThreeNumbers(){
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        assertEquals(3, maximumOfNumbers.max(numbers));
    }
}
