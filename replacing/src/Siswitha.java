import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.lang.String;

public class Siswitha {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>(Arrays.asList(1,2,3,5,8,13,21));

        for(int i = 0; i < numbers.size(); i++){
            if (numbers.get(i)/3 == 0 && numbers.get(i)/5 == 0){
                numbers.set(i, 111222);
            }
            else  if(numbers.get(i)/3 == 0){
                numbers.set(i, 111);
            }
            else  if(numbers.get(i)/5 == 0){
                numbers.set(i, 222);
            }
        }
    }
}
